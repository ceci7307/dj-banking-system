from django.contrib import admin
from django.urls import path
from . import views

app_name = 'banking_system_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('bank/', views.bank, name='bank'),
    path('sign_up/', views.sign_up, name='sign_up'),
    path('<int:pk>/',views.single_customer,name='single_customer'),
]

