# Generated by Django 3.1.2 on 2020-10-28 20:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('banking_system_app', '0007_userprofile'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='mobile',
            new_name='phone',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='email',
        ),
    ]
