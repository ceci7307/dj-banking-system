# Generated by Django 3.1.2 on 2020-10-26 20:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('banking_system_app', '0002_auto_20201024_1346'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Customer',
            new_name='Account',
        ),
    ]
