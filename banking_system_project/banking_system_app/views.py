from django.shortcuts import render, get_object_or_404, reverse
from django.contrib.auth.models import User
from .models import Account
from django.http import HttpResponseRedirect
from . import models
from .models import UserProfile
from django.contrib.auth.decorators import login_required

@login_required
def index(request):
    if request.method == 'POST':
        Account.create(request.user, request.POST['text'])

    accounts = Account.objects.all().filter(user=request.user)
    context = {
        'accounts': accounts,
    }
    return render (request, 'banking_system_app/index.html', context)

@login_required
def bank(request):
    if request.method == "POST":
        text = request.POST["text"]
        user = User()
        user.username = username
        user.save()

    users = User.objects.all()
    context = {
        'users' : users,
    }
    return render (request, 'banking_system_app/bank.html', context)

@login_required
def sign_up(request):
    context = {}
    if request.method == 'POST':
        username = request.POST['username']
        first_name = request.POST['firstname']
        last_name = request.POST['lastname']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        email = request.POST['email']

        phone = request.POST['phone']
        role = request.POST['role']
        membership = request.POST['membership']

        if password == confirm_password:
            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            if user:
                userprofile = UserProfile()
                userprofile.user = user
                userprofile.phone = phone
                userprofile.role = role
                userprofile.membership = membership
                userprofile.save()
                return HttpResponseRedirect(reverse('banking_system_app:bank'))
            else:
                context = {'error': 'Could not create user - try something else'}
        else:
            context ={'error': 'Passwords do not match.'}
    return render(request, 'banking_system_app/sign_up.html', context)

@login_required
def single_customer(request, pk):
    user = User.objects.get(pk=pk)
    context = {
        'user': user,
    }
    return render(request, 'banking_system_app/single_customer.html', context)