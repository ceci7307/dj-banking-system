from django.apps import AppConfig


class BankingSystemAppConfig(AppConfig):
    name = 'banking_system_app'
