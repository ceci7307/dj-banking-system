from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=250)

    @classmethod
    def create(cls, user, text):
        account = cls()
        account.user = user
        account.text = text
        account.save()
    
    def __str__(self):
        return f"{self.text}"

class UserProfile (models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.IntegerField(default = 0)
    membership = models.CharField(max_length = 50, default = None)
    role = models.CharField(max_length = 50, default = None)

    @classmethod
    def create(cls, user):
        userprofile = cls()
        userprofile.user = user
        userprofile.save() 

    def __str__(self):
        return f"{self.user}"


class Loan (models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.IntegerField(default = 0)
    loantype = models.CharField(max_length = 50, default = None)

    @classmethod
    def create(cls, user):
        loan = cls()
        loan.user = user
        loan.save() 

    def __str__(self):
        return f"{self.user}"

class Transaction (models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.IntegerField(default = 0)
    balance = models.IntegerField(default = 0)
    date = models.DateField()
    transactiontype = models.CharField(max_length = 50, default = None)

    @classmethod
    def create(cls, account):
        transaction = cls()
        transaction.account = account
        transaction.save() 

    def __str__(self):
        return f"{self.account}"