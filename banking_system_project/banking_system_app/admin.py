from django.contrib import admin

# Register your models here.
from .models import Account
from .models import UserProfile
from .models import Loan
from .models import Transaction

admin.site.register(Account)
admin.site.register(UserProfile)
admin.site.register(Loan)
admin.site.register(Transaction)